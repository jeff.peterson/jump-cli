#!/bin/sh

# Copyright (c) 2024 Jeff Peterson. Licensed under the WTFPL license, Version 2.

# jump (cd) to a directory using an alias

# Attempts to run in zsh and bash
#   - array indexing (${array[@]:offset:length})
#   - read array (-a vs -A)

_jump_cli() {

  _jump_err() {
    echo "ERROR: $*" >&2
  }


  if [ -n "$ZSH_VERSION" ]; then
    local -r _jump_array_read_flag="-A"
  else
    # bash, fish, etc
    local -r _jump_array_read_flag="-a"
  fi


  local _JUMPLIST=
  if [ $JUMPLIST ]; then
    _JUMPLIST=$JUMPLIST
  elif [ -f $XDG_STATE_HOME/jump-cli/.jumplist ]; then
    _JUMPLIST=$XDG_STATE_HOME/jump-cli/.jumplist
  else
    _JUMPLIST="$HOME/.jumplist"
    [ ! -f "$_JUMPLIST" ] && touch "$_JUMPLIST"
  fi
  readonly _JUMPLIST

  if [ ! -f "$_JUMPLIST" ]; then
    _jump_err "jumplist is not a file: $_JUMPLIST"
  fi


  _jump_safe_sed() {
    local -r _unamestr=$(uname)
    if [ "$_unamestr" = 'Darwin' ] || [ "$_unamestr" = 'FreeBSD' ]; then
      sed -i '' "$1" "$2"
      return $?
    else
      sed -i "$1" "$2"
      return $?
    fi
  }


  local _jump_alias=
  local _jump_path=
  _jump_find_in_jumplist() {
    while IFS= read -r line; do
      IFS="=" read $_jump_array_read_flag entry <<< "$line"
      if [ "$1" = "${entry[@]:0:1}" ]; then
        _jump_alias="${entry[@]:0:1}"
        _jump_path="${entry[@]:1:1}"
        break
      fi
    done < "$_JUMPLIST"
  }


  _jump_add() {
    case $1 in 
      *=*)
        _jump_err "ALIAS cannot contain '='"
        ;;
      add | remove | list)
        _jump_err "ALIAS cannot be COMMAND ('add', 'remove', 'list')"
        ;;
    esac

    local -r _jump_new_alias=$1
    shift
    local -r _jump_new_path="${@:-$PWD}"

    if [ ! -d "$_jump_new_path" ]; then
      echo "WARNING: '$_jump_new_path' is not a directory. Adding..." >&2
    fi

    local -r _jump_new_entry="$_jump_new_alias=$_jump_new_path"

    _jump_find_in_jumplist $_jump_new_alias

    if [ $_jump_alias ]; then
      local _jump_escaped_new_entry=$(sed 's/\\/\\\\/g' <<< $_jump_new_entry)
      _jump_escaped_new_entry=$(sed 's/\//\\\//g' <<< $_jump_escaped_new_entry)
      _jump_safe_sed "s/^${_jump_new_alias}=.*$/${_jump_escaped_new_entry}/g" "$_JUMPLIST"
    else
      echo "$_jump_new_entry" >> "$_JUMPLIST"
    fi
  }


  _jump_remove() {
    _jump_find_in_jumplist $1
    _jump_safe_sed "/^${_jump_alias}=/d" "$_JUMPLIST"
  }


  _jump_list() {
    cat "$_JUMPLIST"
  }


  _jump() {
    _jump_find_in_jumplist $1
    if [ ! $_jump_alias ]; then
      _jump_err "'$1' is not a valid ALIAS."
    elif [ ! -d $_jump_path ]; then
      _jump_err "'$_jump_path' is not a valid DIRECTORY."
    else
      builtin cd $_jump_path
    fi
  }


  _jump_print_help() {
    cat << EOF
  Usage: jump [OPTIONS] ALIAS
  Usage: jump [OPTIONS] COMMAND ALIAS [DIRECTORY]

    Simple directory bookmarking tool to save keystrokes with 'cd'. 

  Options:
    --help                Show this message and exit.

  Commands:
    add ALIAS [DIRECTORY] Add alias and path to '.jumplist'.
                          Default: current directory.
                          Prohibited aliases: 'add', 'remove', 'list'.
                          Aliases containing '=' prohibited.
    remove ALIAS          Remove alias and path from '.jumplist'.
    list                  Print '.jumplist'.

  Environment Variables:
    \$JUMPLIST      Specify a custom '.jumplist' file location.
    \$JUMP_CMD      Specify a custom jump command alias.

  Notes:
    Aliases are stored in file '.jumplist'. File location (in order):
      \$JUMPLIST
      \$XDG_STATE_HOME/jump-cli/.jumplist
      \$HOME/.jumplist

  Examples:
      jump add foo ~/path   Alias directory ~/path with foo.
      jump add bar          Alias current directory with bar.
      jump foo              cd to ~/path.
      jump remove foo       Remove alias foo.
      jump list             Print aliases and paths.
      jump --help           Show this message and exit.
EOF
  }


  case $1 in
    "add")
      shift
      _jump_add "$@"
      ;;

    "remove")
      shift
      _jump_remove $1
      ;;

    "list")
      _jump_list
      ;;

    "--help")
      _jump_print_help
      ;;

    "")
      _jump_err "directory ALIAS or COMMAND ('add', 'remove', 'list') required."
      ;;

    *)
      _jump $1
      ;;
  esac

}


builtin alias ${JUMP_CMD:-jump}=_jump_cli
